package uz.infinityandro.wallpaper.dagger

import org.koin.dsl.module
import uz.infinityandro.wallpaper.repository.Impl.WallpaperRepositoryImpl
import uz.infinityandro.wallpaper.repository.WallpaperRepository
import uz.infinityandro.wallpaper.ui.activity.activityviewmodel.activityrepos.SavedImageRepos
import uz.infinityandro.wallpaper.ui.activity.activityviewmodel.activityrepos.impl.SavedImageReposImpl

val repositoryModule= module {
    factory <WallpaperRepository>{ WallpaperRepositoryImpl()}
    factory <SavedImageRepos>{ SavedImageReposImpl(context = get ()) }
}