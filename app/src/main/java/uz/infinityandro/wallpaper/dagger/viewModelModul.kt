package uz.infinityandro.wallpaper.dagger

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import uz.infinityandro.wallpaper.ui.activity.activityviewmodel.PhotoActivityViewModel
import uz.infinityandro.wallpaper.viewmodel.Impl.WallpaperViewModelImpl

val viewModelModule= module {
    viewModel { WallpaperViewModelImpl(wallpaperRepository = get ()) }
    viewModel { PhotoActivityViewModel(context = get (),savedImageRepos = get()) }
}