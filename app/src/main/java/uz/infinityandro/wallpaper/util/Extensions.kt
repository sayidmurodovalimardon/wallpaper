package uz.infinityandro.wallpaper.util

import android.content.Context
import android.widget.Toast

fun Context.displayToast(s:String){
    Toast.makeText(this, "$s", Toast.LENGTH_SHORT).show()
}