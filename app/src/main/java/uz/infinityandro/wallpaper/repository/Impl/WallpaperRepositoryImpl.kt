package uz.infinityandro.wallpaper.repository.Impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import uz.infinityandro.wallpaper.newtork.api.ApiClient
import uz.infinityandro.wallpaper.newtork.api.ApiService
import uz.infinityandro.wallpaper.repository.WallpaperRepository
import uz.infinityandro.wallpaper.ui.model.Photo

class WallpaperRepositoryImpl:WallpaperRepository {
    override fun getPhotos(id:String,page: Int, per_page: Int): Flow<Result<List<Photo>>> = flow {
        var api = ApiClient.getBooks().create(ApiService::class.java)
        val response=api.getCurrency(id,page, per_page)
        if (response.isSuccessful){
            emit(Result.success(response.body()!!))
        }else{
            emit(Result.failure(Throwable("Error")))
        }
    }.flowOn(Dispatchers.Main)
}