package uz.infinityandro.wallpaper.repository

import kotlinx.coroutines.flow.Flow
import uz.infinityandro.wallpaper.ui.model.Photo

interface WallpaperRepository {
    fun getPhotos(id:String,page: Int, per_page: Int): Flow<Result<List<Photo>>>
}