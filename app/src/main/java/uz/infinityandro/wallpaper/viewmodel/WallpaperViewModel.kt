package uz.infinityandro.wallpaper.viewmodel

import androidx.lifecycle.LiveData
import uz.infinityandro.wallpaper.ui.model.Photo

interface WallpaperViewModel {
    val errorMessageLiveData: LiveData<String>
    val connectionLiveData: LiveData<Boolean>
    val allDataLiveData: LiveData<List<Photo>>
    val progressLiveData: LiveData<Boolean>

    fun getPhoto(page:Int)
}