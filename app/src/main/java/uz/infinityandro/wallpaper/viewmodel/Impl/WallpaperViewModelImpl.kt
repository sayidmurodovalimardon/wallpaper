package uz.infinityandro.wallpaper.viewmodel.Impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import uz.infinityandro.wallpaper.newtork.connection.isConnected
import uz.infinityandro.wallpaper.repository.WallpaperRepository
import uz.infinityandro.wallpaper.ui.model.Photo
import uz.infinityandro.wallpaper.util.Constants
import uz.infinityandro.wallpaper.viewmodel.WallpaperViewModel
import kotlin.random.Random

class WallpaperViewModelImpl(private val wallpaperRepository: WallpaperRepository):ViewModel(),WallpaperViewModel {
    override val errorMessageLiveData= MutableLiveData<String>()
    override val connectionLiveData=MutableLiveData<Boolean>()
    override val allDataLiveData=MutableLiveData<List<Photo>>()
    override val progressLiveData=MutableLiveData<Boolean>()


    override fun getPhoto(page: Int) {
        if (!isConnected()){
            connectionLiveData.postValue(true)
            return
        }
        connectionLiveData.postValue(false)
        wallpaperRepository.getPhotos(Constants.ACCESS_KEYS[Random.nextInt(0, Constants.ACCESS_KEYS.size)],page,Constants.PER_PAGE).onEach {
            progressLiveData.postValue(true)
            it.onSuccess {
                allDataLiveData.postValue(it)
                progressLiveData.postValue(false)
            }.onFailure {
                errorMessageLiveData.postValue(it.message)
            }
        }.launchIn(viewModelScope)
    }
}