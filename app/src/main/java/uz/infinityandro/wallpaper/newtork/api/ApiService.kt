package uz.infinityandro.wallpaper.newtork.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import uz.infinityandro.wallpaper.ui.model.Photo

interface ApiService {
    @GET("photos")
    suspend fun getCurrency(
        @Query("client_id") client_id:String,
        @Query("page") page: Int,
        @Query("per_page") per_page: Int,
    ): Response<List<Photo>>
}