package uz.infinityandro.wallpaper.newtork.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uz.infinityandro.wallpaper.util.Constants

object ApiClient {
    var retrofit: Retrofit?=null

    fun getBooks(): Retrofit {
        retrofit= Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("${Constants.url}")
            .build()

        return retrofit!!
    }
}