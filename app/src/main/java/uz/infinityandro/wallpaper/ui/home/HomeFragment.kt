package uz.infinityandro.wallpaper.ui.home

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.skydoves.powermenu.CircularEffect
import com.skydoves.powermenu.OnMenuItemClickListener
import com.skydoves.powermenu.PowerMenu
import com.skydoves.powermenu.PowerMenuItem
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.wallpaper.R
import uz.infinityandro.wallpaper.databinding.FragmentHomeBinding
import uz.infinityandro.wallpaper.ui.activity.PhotoActivity
import uz.infinityandro.wallpaper.ui.adapter.WallpaperAdapter
import uz.infinityandro.wallpaper.ui.model.Photo
import uz.infinityandro.wallpaper.viewmodel.Impl.WallpaperViewModelImpl
import java.io.File
import java.io.Serializable

class HomeFragment : Fragment() {
    private val viewModel: WallpaperViewModelImpl by viewModel()
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding
    private var powerMenu: PowerMenu? = null
    lateinit var list: ArrayList<Photo>
    lateinit var adapter: WallpaperAdapter
    var currentPage = 1
    var currentPos = 0


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        binding= FragmentHomeBinding.inflate(inflater, container, false)
        list = ArrayList()
        list.clear()
        loadPhoto()
        currentPage = 1
        currentPos = 0
        loadData()
        val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        layoutManager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
        binding.recycler.layoutManager = layoutManager
        binding.recycler.setHasFixedSize(true)
        adapter = WallpaperAdapter(list) {
     Intent(requireContext(),PhotoActivity::class.java).let { intent->
     intent.putExtra("photo",it as Serializable)
     startActivity(intent)

     }
        }
        binding.recycler.adapter = adapter

        binding.recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!binding.recycler.canScrollVertically(1)) {
                    binding.progress.visibility=View.VISIBLE
                    currentPage++
                    loadData()
                }
            }
        })

        binding.retry.setOnClickListener {
            loadData()
        }
        binding.menu.setOnClickListener {
            powerMenu = PowerMenu.Builder(requireContext())
                .addItem(PowerMenuItem("Clear Cache", false))
                .addItem(PowerMenuItem("Share App", false))
                .setCircularEffect(CircularEffect.BODY)
                .setMenuRadius(8f)
                .setMenuShadow(8f)
                .setTextColor(resources.getColor(R.color.black))
                .setTextSize(16)
                .setTextGravity(Gravity.START)
                .setMenuColor(resources.getColor(R.color.white))
                .setOnMenuItemClickListener(onMenuItemClickListener)
                .setTextTypeface(Typeface.defaultFromStyle(Typeface.NORMAL))
                .build()
            powerMenu!!.showAsDropDown(binding.menu)
        }
        return binding.root
    }

    private fun loadPhoto() {
        viewModel.connectionLiveData.observe(requireActivity(), {
            if (it) {
                binding.header.visibility = View.GONE
                binding.network.visibility = View.VISIBLE
            } else {
                binding.header.visibility = View.VISIBLE
                binding.network.visibility = View.GONE
            }
        })
        viewModel.progressLiveData.observe(requireActivity(), {
            binding.progress.visibility = if (it) View.VISIBLE else View.GONE
        })
        viewModel.allDataLiveData.observe(requireActivity(), {
            if (it != null) {
                val oldPos = list.size
                list.addAll(it)
                adapter.notifyItemRangeInserted(oldPos, list.size)
            }
        })
    }

    private fun loadData() {
        viewModel.getPhoto(currentPage)
    }


    private val onMenuItemClickListener: OnMenuItemClickListener<PowerMenuItem?> =
        OnMenuItemClickListener<PowerMenuItem?> { position, item ->
            powerMenu!!.selectedPosition = position
            if (powerMenu!!.selectedPosition == 0) {
                powerMenu!!.dismiss()
                val builder = android.app.AlertDialog.Builder(requireContext())
                    .setTitle("Clear Cache")
                    .setMessage("Do you want to Clear Cache?")
                    .setCancelable(false)
                    .setPositiveButton("Yes") { dialogInterface: DialogInterface, _: Int ->
                        try {
                            val dir: File = requireActivity().cacheDir
                            deleteDir(dir)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    .setNegativeButton("No") { dialogInterface: DialogInterface, _: Int ->
                    }
                val create = builder.create()
                create.setCanceledOnTouchOutside(false)
                create.show()

            } else if (powerMenu!!.selectedPosition == 1) {
                powerMenu!!.dismiss()
                val dialog = AlertDialog.Builder(requireContext()).create()
                dialog.setTitle("Do you want to share this app?")
                dialog.setCanceledOnTouchOutside(false)
                dialog.setButton(
                    AlertDialog.BUTTON_POSITIVE,
                    "Yes",
                    DialogInterface.OnClickListener { dialog, which ->
                        val intent = Intent(Intent.ACTION_SEND)
                        intent.type = "text/plain"
                        val url = ""
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Wallpaper App")
                        intent.putExtra(Intent.EXTRA_TEXT, url)
                        startActivity(intent)
                    })
                dialog.setButton(
                    AlertDialog.BUTTON_NEGATIVE,
                    "No",
                    DialogInterface.OnClickListener { dialog, which ->
                        dialog.dismiss()
                    })
                dialog.show()


            }
        }
    private fun deleteDir(dir: File?): Boolean {
        return if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
            dir.delete()
        } else if (dir != null && dir.isFile) {
            dir.delete()
        } else {
            false
        }
    }

    override fun onStart() {
        super.onStart()
        binding.recycler.layoutManager!!.scrollToPosition(currentPos)
    }
}