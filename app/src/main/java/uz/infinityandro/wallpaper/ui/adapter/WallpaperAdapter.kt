package uz.infinityandro.wallpaper.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import uz.infinityandro.wallpaper.databinding.ItemAdapterwallpaperBinding
import uz.infinityandro.wallpaper.ui.model.Photo

class WallpaperAdapter(var list: List<Photo>,var listener:(photo:Photo)->Unit):RecyclerView.Adapter<WallpaperAdapter.VH>() {
    inner class VH(var binding: ItemAdapterwallpaperBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(photo: Photo) {
            binding.root.setOnClickListener {
                listener(photo)
            }
            Glide.with(binding.root).load(photo.urls.small).diskCacheStrategy(DiskCacheStrategy.NONE).into(binding.image)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemAdapterwallpaperBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}