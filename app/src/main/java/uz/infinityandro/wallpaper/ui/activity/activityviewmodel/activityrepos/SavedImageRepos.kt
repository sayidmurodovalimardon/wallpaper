package uz.infinityandro.wallpaper.ui.activity.activityviewmodel.activityrepos

import android.graphics.Bitmap
import android.net.Uri
import java.io.File

interface SavedImageRepos {
    suspend fun saveImage(url:String)
}