package uz.infinityandro.wallpaper.ui.activity.activityviewmodel

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import uz.infinityandro.wallpaper.ui.activity.activityviewmodel.activityrepos.SavedImageRepos
import uz.infinityandro.wallpaper.util.Coroutine

class PhotoActivityViewModel(
    private val context: Context,
    private val savedImageRepos: SavedImageRepos
) : ViewModel() {
  fun save(url:String){
   Coroutine.io {

       savedImageRepos.saveImage(url)
   }
  }
}