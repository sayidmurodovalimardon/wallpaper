package uz.infinityandro.wallpaper.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.WallpaperManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.wallpaper.R
import uz.infinityandro.wallpaper.databinding.ActivityPhotoBinding
import uz.infinityandro.wallpaper.ui.activity.activityviewmodel.PhotoActivityViewModel
import uz.infinityandro.wallpaper.ui.model.Photo
import uz.infinityandro.wallpaper.util.displayToast
import java.io.ByteArrayOutputStream
import java.net.URL

class PhotoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPhotoBinding
    private val viewModel:PhotoActivityViewModel by viewModel()
    private lateinit var uri: Uri
    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityPhotoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        val photo=intent.getSerializableExtra("photo") as Photo
        Glide.with(applicationContext).load(photo.urls.full).centerCrop().listener(
            @SuppressLint("CheckResult")
            object :RequestListener<Drawable>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                 return true
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.progressBar.visibility=View.GONE
                    return false
                }

            }
        ).into(binding.image)
        listeners(photo)
        viewModelListeners()
    }

    private fun viewModelListeners() {

    }

    private fun getPhotoInScreen() {
        val bitmap: Bitmap = binding.image.drawable.toBitmap()
        uri=getImageUri(bitmap)
    }

    private fun getImageUri(bitmap: Bitmap):Uri {
        val bytes=ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,bytes)
        val path=MediaStore.Images.Media.insertImage(application.contentResolver,bitmap,"Hi",null)
        return Uri.parse(path)
    }

    private fun listeners(photo: Photo) {

        binding.backBtnContainer.setOnClickListener {
            onBackPressed()
        }
        binding.downloadWallpaperBtnContainer.setOnClickListener {
            download()
        }
        binding.shareWallpaperBtnContainer.setOnClickListener {
            share()
        }
        binding.setWallpaperBtnContainer.setOnClickListener {
            setWallpaperbackground(photo)
        }
    }
    @SuppressLint("MissingPermission")
    private fun setWallpaperbackground(photo: Photo) {
        binding.setWallpaperBtnContainer.setCardBackgroundColor(resources.getColor(R.color.white))
        binding.setWallpaperText.text="Wallpaper Set"
        binding.setWallpaperText.setTextColor(resources.getColor(R.color.black))
        val bitmap: Bitmap = binding.image.drawable.toBitmap()
        val task: SetWallpaperTask = SetWallpaperTask(applicationContext, bitmap)
        task.execute(true)
        Toast.makeText(applicationContext, "Image had set", Toast.LENGTH_SHORT).show()

    }
    companion object {
        class SetWallpaperTask internal constructor(
            private val context: Context,
            private val bitmap: Bitmap
        ) :
            AsyncTask<Boolean, String, String>() {
            override fun doInBackground(vararg params: Boolean?): String {
                val wallpaperManager: WallpaperManager = WallpaperManager.getInstance(context)
                wallpaperManager.setBitmap(bitmap)
                return "Wallpaper Set"
            }

        }
    }


    private fun share() {
        getPhotoInScreen()
        with(Intent(Intent.ACTION_SEND)){
            putExtra(Intent.EXTRA_STREAM,uri)
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            type="image/*"
            startActivity(this)
        }

    }

    private fun download() {
    Dexter.withContext(applicationContext)
        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        .withListener(object :PermissionListener{
            override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                val bitmap= binding.image.drawable.toBitmap()

                val uri=getImageUri(bitmap)
                viewModel.save(uri.toString())
                Toast.makeText(applicationContext, "Image Saved to Gallery", Toast.LENGTH_SHORT).show()


            }

            override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                displayToast("Permission denied")
            }

            override fun onPermissionRationaleShouldBeShown(
                p0: PermissionRequest?,
                p1: PermissionToken?
            ) {

            }

        }).check()

    }

    override fun onStop() {
        super.onStop()

    }

}