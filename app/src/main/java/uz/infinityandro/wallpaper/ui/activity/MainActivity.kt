package uz.infinityandro.wallpaper.ui.activity

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import uz.infinityandro.wallpaper.R
import uz.infinityandro.wallpaper.newtork.api.ApiClient
import uz.infinityandro.wallpaper.newtork.api.ApiService
import uz.infinityandro.wallpaper.ui.model.Photo
import uz.infinityandro.wallpaper.util.Constants
import uz.infinityandro.wallpaper.util.displayToast
import uz.infinityandro.wallpaper.viewmodel.Impl.WallpaperViewModelImpl

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)

    }
}